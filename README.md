**Bệnh lậu** là một bệnh lây truyền qua đường tình dục do vi khuẩn Neisseria Gonorrhoeae gây ra , lây nhiễm vào niêm mạc niệu đạo, cổ tử cung, trực tràng và cổ họng hoặc màng bao phủ phần trước của mắt (kết mạc và giác mạc).

- Bệnh lậu thường lây lan qua quan hệ tình dục.
- Mọi người thường có dịch tiết ra từ dương vật hoặc âm đạo và có thể cần đi tiểu thường xuyên và khẩn cấp hơn.
- Hiếm khi, bệnh lậu lây nhiễm vào khớp, da hoặc tim.
- Kiểm tra bằng kính hiển vi, nuôi cấy hoặc xét nghiệm DNA của một mẫu chất thải hoặc xét nghiệm DNA của nước tiểu có thể phát hiện nhiễm trùng.
- Thuốc kháng sinh có thể chữa nhiễm trùng, nhưng tình trạng kháng kháng sinh được sử dụng để điều trị bệnh lậu đang trở nên phổ biến hơn.

##Bệnh lậu là gì?

Bệnh lậu hầu như luôn lây lan qua quan hệ tình dục. Sau một giai đoạn giao hợp âm đạo mà không có bao cao su, cơ hội lây lan từ người phụ nữ bị nhiễm sang người đàn ông là khoảng 20%. Cơ hội lây lan từ một người đàn ông bị nhiễm sang một người phụ nữ có thể cao hơn.

Nếu phụ nữ mang thai bị nhiễm bệnh, vi khuẩn có thể lây lan sang mắt của thai nhi trong khi sinh, gây viêm kết mạc ở trẻ sơ sinh . Tuy nhiên, ở hầu hết các nước phát triển, việc ngăn ngừa nhiễm trùng vì tất cả trẻ sơ sinh được điều trị thường xuyên sau khi sinh bằng thuốc mỡ mắt.

Nhiều người mắc bệnh lậu có các bệnh lây truyền qua đường tình dục khác (STDs), chẳng hạn như nhiễm chlamydia , giang mai hoặc nhiễm virus suy giảm miễn dịch ở người (HIV) .

##Triệu chứng dẫn đến lậu

Thông thường, bệnh lậu chỉ gây ra các triệu chứng tại các vị trí nhiễm trùng ban đầu. Ở một số ít người, nhiễm trùng lây lan qua dòng máu đến các bộ phận khác của cơ thể, đặc biệt là da, khớp hoặc cả hai.

Một số nam giới (khoảng 25%) có các triệu chứng tối thiểu. Các triệu chứng bắt đầu trong vòng khoảng 2 đến 14 ngày sau khi nhiễm bệnh. Đàn ông cảm thấy khó chịu nhẹ ở niệu đạo (ống dẫn nước tiểu từ bàng quang ra khỏi cơ thể). Sự khó chịu này được theo dõi một vài giờ sau đó bởi cơn đau từ nhẹ đến nặng khi đi tiểu, dịch mủ màu vàng xanh từ dương vật và thường xuyên đi tiểu. Lỗ mở ở đầu dương vật có thể bị đỏ và sưng. Các vi khuẩn đôi khi lây lan đến mào tinh hoàn (ống cuộn trên đỉnh của mỗi tinh hoàn), làm cho bìu sưng lên và cảm thấy mềm khi chạm vào.

Một số phụ nữ (khoảng 10 đến 20%) có các triệu chứng tối thiểu hoặc không có. Do đó, bệnh lậu chỉ có thể được phát hiện trong quá trình sàng lọc thường quy hoặc sau khi chẩn đoán nhiễm trùng ở bạn tình nam. Các triệu chứng thường không bắt đầu cho đến ít nhất 10 ngày sau khi nhiễm bệnh. Một số phụ nữ chỉ cảm thấy khó chịu nhẹ ở vùng sinh dục và có dịch tiết ra từ âm đạo. Tuy nhiên, những phụ nữ khác có các triệu chứng nghiêm trọng hơn, chẳng hạn như đi tiểu thường xuyên và đau khi đi tiểu. Những triệu chứng này phát triển khi niệu đạo cũng bị nhiễm trùng.

Vi khuẩn thường lây lan lên đường sinh dục và lây nhiễm các ống nối buồng trứng với tử cung (ống dẫn trứng). Nhiễm trùng này, được gọi là viêm salping, gây đau bụng dưới nghiêm trọng, đặc biệt là trong khi giao hợp. Ở một số phụ nữ, nhiễm trùng lan đến niêm mạc khoang bụng (phúc mạc), gây viêm phúc mạc hoặc viêm vùng chậu , có thể gây đau dữ dội ở vùng bụng dưới. Phụ nữ đã bị bệnh viêm vùng chậu có nguy cơ vô sinh và mang thai sai lệch (ngoài tử cung) , có thể gây chảy máu bên trong nguy hiểm.

Thỉnh thoảng, nhiễm trùng trong bụng tập trung quanh gan. Nhiễm trùng này, được gọi là viêm màng phổi hoặc hội chứng Fitz-Hugh-Curtis, gây đau ở phần trên bên phải của bụng. Nó xảy ra chủ yếu ở phụ nữ.

***Quan hệ tình dục qua đường hậu môn*** với một đối tác bị nhiễm bệnh có thể dẫn đến bệnh lậu trực tràng. Nhiễm trùng này thường không gây ra triệu chứng, nhưng nó có thể làm cho nhu động ruột bị đau. Các triệu chứng khác bao gồm táo bón, ngứa, chảy máu và chảy dịch từ trực tràng. Khu vực xung quanh hậu môn có thể trở nên đỏ và thô, và phân có thể được phủ bằng chất nhầy và mủ. Khi bác sĩ kiểm tra trực tràng bằng ống quan sát (ống soi), có thể nhìn thấy chất nhầy và mủ trên thành trực tràng.

***Quan hệ tình dục bằng miệng*** với một đối tác bị nhiễm bệnh có thể dẫn đến bệnh lậu họng (viêm họng do lậu cầu). Thông thường, những nhiễm trùng này không gây ra triệu chứng, nhưng cổ họng có thể bị đau.

Nếu chất lỏng bị nhiễm bệnh tiếp xúc với mắt, viêm kết mạc do lậu cầu có thể phát triển, gây sưng mí mắt và chảy mủ từ mắt. Ở người lớn, thường chỉ có một mắt bị nhiễm bệnh. Trẻ sơ sinh thường bị nhiễm trùng ở cả hai mắt. Mù có thể xảy ra nếu nhiễm trùng không được điều trị sớm.

Ở trẻ em, bệnh lậu thường là kết quả của lạm dụng tình dục. Ở các bé gái, vùng sinh dục (âm hộ) có thể bị kích thích, đỏ và sưng, và chúng có thể có dịch tiết ra từ âm đạo. Nếu niệu đạo bị nhiễm trùng, trẻ em, chủ yếu là bé trai, có thể bị đau khi đi tiểu.

Hiếm khi, ***nhiễm trùng lậu cầu lan tỏa*** (hội chứng viêm khớp-viêm da) phát triển. Nó xảy ra khi nhiễm trùng lây lan qua máu đến các bộ phận khác của cơ thể, đặc biệt là da và khớp. Khớp trở nên sưng, đau và vô cùng đau đớn, hạn chế vận động. Da trên khớp bị nhiễm trùng có thể đỏ và ấm. Mọi người thường bị sốt, thường cảm thấy bị bệnh và bị viêm khớp ở một hoặc nhiều khớp. Những đốm nhỏ, đỏ có thể xuất hiện trên da, thường là trên cánh tay và chân. Các đốm hơi đau và có thể chứa đầy mủ. Nhiễm trùng khớp, máu và tim có thể được điều trị, nhưng sự phục hồi từ viêm khớp có thể chậm.

Viêm khớp nhiễm khuẩn cầu khuẩn là một dạng nhiễm trùng lậu cầu lan rộng gây viêm khớp đau đớn. Thông thường, nó ảnh hưởng đến một hoặc hai khớp lớn, chẳng hạn như đầu gối, mắt cá chân, cổ tay hoặc khuỷu tay, Các triệu chứng thường bắt đầu đột ngột. Mọi người thường bị sốt. Các khớp bị nhiễm trùng là đau và sưng, và cử động bị hạn chế. Da trên các khớp bị nhiễm trùng có thể ấm và đỏ.

##Chẩn đoán bệnh lậu

Kiểm tra dưới kính hiển vi hoặc trong phòng thí nghiệm

Trong hơn 95% nam giới bị nhiễm bệnh xuất viện, bệnh lậu có thể được chẩn đoán trong vòng một giờ bằng cách xác định vi khuẩn (gonococci) trong các mẫu dịch tiết được kiểm tra dưới kính hiển vi. Nếu xuất tiết rõ ràng, các bác sĩ chạm vào một miếng gạc hoặc trượt đến cuối dương vật để lấy mẫu. Nếu không có dịch tiết rõ ràng, các bác sĩ sẽ nhét một miếng gạc nhỏ từ nửa inch trở lên vào niệu đạo để lấy mẫu. Đàn ông được yêu cầu kiềm chế đi tiểu ít nhất 2 giờ trước khi lấy mẫu.

Việc xác định vi khuẩn trong một mẫu dịch tiết ra từ cổ tử cung là khó khăn hơn. Các vi khuẩn có thể được nhìn thấy chỉ trong khoảng một nửa số phụ nữ bị nhiễm bệnh.

Mẫu (từ niệu đạo hoặc cổ tử cung) cũng được gửi đến phòng thí nghiệm để nuôi cấy (để phát triển các sinh vật) và cho các xét nghiệm khác. Các xét nghiệm như vậy rất đáng tin cậy ở cả hai giới nhưng mất nhiều thời gian hơn so với kiểm tra bằng kính hiển vi. Nếu bác sĩ nghi ngờ nhiễm trùng cổ họng, trực tràng hoặc máu, các mẫu từ các khu vực này được gửi đi xét nghiệm trong phòng thí nghiệm.

Các xét nghiệm có độ nhạy cao có thể được thực hiện để phát hiện DNA của gonococci và chlamydiae (thường có mặt). Các phòng thí nghiệm có thể kiểm tra cả hai bệnh nhiễm trùng trong một mẫu. Đối với một số thử nghiệm này (được gọi là xét nghiệm khuếch đại axit nucleic hoặc NAATS), các kỹ thuật làm tăng lượng vật liệu di truyền của vi khuẩn được sử dụng. Bởi vì các kỹ thuật này làm cho các sinh vật dễ dàng phát hiện hơn, mẫu nước tiểu có thể được sử dụng. Do đó, các xét nghiệm này thuận tiện cho việc sàng lọc những người đàn ông và phụ nữ không có triệu chứng hoặc không muốn lấy mẫu chất lỏng lấy từ bộ phận sinh dục của họ.

Bởi vì nhiều người có nhiều hơn một STD, các bác sĩ có thể kiểm tra các mẫu máu và dịch sinh dục cho các STD khác, chẳng hạn như giang mai và nhiễm HIV. Các bác sĩ cũng kiểm tra nhiễm trùng chlamydia.

Nếu khớp bị đỏ và sưng, các bác sĩ sẽ lấy chất lỏng từ khớp bằng kim. Các chất lỏng được gửi cho văn hóa và các xét nghiệm khác.

###Sàng lọc bệnh lậu

Một số người không có triệu chứng được sàng lọc bệnh lậu vì họ có những đặc điểm làm tăng nguy cơ nhiễm trùng này.

Ví dụ, phụ nữ không mang thai được sàng lọc nếu họ:

- Từ 24 tuổi trở xuống và hoạt động tình dục
- Đã có STD trước đó
- Tham gia vào các hoạt động tình dục rủi ro (như có nhiều bạn tình, không sử dụng bao cao su thường xuyên hoặc tham gia hoạt động mại dâm)
- Có một đối tác tình dục tham gia vào các hoạt động tình dục rủi ro

Phụ nữ mang thai được sàng lọc trong lần khám thai đầu tiên của họ và, nếu họ có các yếu tố nguy cơ nhiễm trùng, một lần nữa trong tam cá nguyệt thứ ba.

Đàn ông dị tính không được kiểm tra thường xuyên trừ khi họ được coi là có nguy cơ nhiễm trùng cao, ví dụ, khi họ có nhiều bạn tình, là bệnh nhân tại một phòng khám vị thành niên hoặc STD, hoặc được đưa vào một cơ sở cải huấn.

Đàn ông quan hệ tình dục với đàn ông chỉ được sàng lọc nếu họ đã hoạt động tình dục trong năm ngoái.

##Phòng ngừa bệnh lậu

Các biện pháp chung sau đây có thể giúp ngăn ngừa bệnh lậu (và các STD khác):

- Sử dụng bao cao su thường xuyên và đúng cách (xem Cách sử dụng bao cao su )
- Tránh các thực hành tình dục không an toàn, chẳng hạn như thay đổi bạn tình thường xuyên hoặc quan hệ tình dục với gái mại dâm hoặc với các đối tác có bạn tình khác
- Chẩn đoán và điều trị nhiễm trùng kịp thời (để tránh lây lan sang người khác)
- Xác định các liên hệ tình dục của những người bị nhiễm bệnh, tiếp theo là tư vấn hoặc điều trị các liên hệ này

Không quan hệ tình dục (hậu môn, âm đạo hoặc miệng) là cách đáng tin cậy nhất để ngăn ngừa STDs nhưng thường không thực tế.

##Điều trị Bệnh lậu

Các kháng sinh ceftriaxone cộng với azithromycin
Xét nghiệm và điều trị bạn tình
Các bác sĩ thường cho những người mắc bệnh lậu một mũi tiêm kháng sinh ceftriaxone vào cơ bắp, cộng với một liều azithromycin bằng đường uống. Đôi khi thay vì azithromycin , các bác sĩ sử dụng doxycycline bằng đường uống, hai lần một ngày trong 1 tuần. Mặc dù ceftriaxone chữa khỏi hầu hết mọi người ở Hoa Kỳ, azithromycin được dùng cùng với ceftriaxone vì thuốc này có thể giúp giữ cho gonococci không bị kháng thuốc. Ngoài ra, azithromycin và doxycycline tiêu diệt chlamydiae, thường xuất hiện ở những người mắc bệnh lậu.

Nếu mọi người bị dị ứng với ceftriaxone , họ được cho dùng liều cao azithromycin và gemifloxacin bằng đường uống hoặc gentamicin tiêm vào cơ bắp.

Nếu bệnh lậu đã lây lan qua máu, mọi người thường được điều trị trong bệnh viện và tiêm kháng sinh tiêm tĩnh mạch hoặc tiêm vào cơ bắp.

Nếu các triệu chứng tái phát hoặc tồn tại sau khi điều trị, các bác sĩ có thể lấy mẫu để nuôi cấy để xác định xem mọi người có được chữa khỏi hay không và có thể làm các xét nghiệm để xác định xem liệu gonococci có kháng với kháng sinh được sử dụng hay không.

Những người mắc bệnh lậu nên kiêng hoạt động tình dục cho đến khi điều trị kết thúc để tránh lây nhiễm cho bạn tình.

###Đối tác tình dục

Tất cả bạn tình có quan hệ tình dục với người nhiễm bệnh trong 60 ngày qua nên được xét nghiệm bệnh lậu và STDs khác, nếu xét nghiệm dương tính, nên được điều trị. Nếu bạn tình tiếp xúc với bệnh lậu trong vòng 2 tuần qua, họ sẽ được điều trị mà không cần chờ kết quả xét nghiệm.

Liệu pháp đối tác khẩn cấp là một lựa chọn bác sĩ đôi khi sử dụng để giúp điều trị dễ dàng hơn cho bạn tình. Cách tiếp cận này liên quan đến việc cho những người mắc bệnh lậu một đơn thuốc hoặc thuốc để cung cấp cho bạn tình của họ. Do đó, bạn tình không cần gặp bác sĩ trước khi được điều trị. Gặp bác sĩ là tốt hơn bởi vì sau đó bác sĩ có thể kiểm tra dị ứng thuốc và sự hiện diện của các STD khác. Tuy nhiên, nếu đối tác không có khả năng gặp bác sĩ, đối tác cấp tốc là hữu ích.